import url from 'url'
import http from 'http'
import { StringDecoder } from 'string_decoder'

const handlers = {}

handlers.sample = (data, callback) => {
  callback(406, { name: 'sample handler' })
}

handlers.notFound = (data, callback) => {
  callback(404)
}

const router = {
  sample: handlers.sample
}

const server = http.createServer((req, res) => {
  // Get the URL and parse it
  let parsedUrl = url.parse(req.url, true)
  // Get the path

  let { pathname: path } = parsedUrl
  let trimmedPath = path.replace(/^\/+|\/+$/g, '')

  let queryStringObject = parsedUrl.query

  let method = req.method.toLowerCase()

  let headers = req.headers

  let decoder = new StringDecoder('utf-8')
  let buffer = ''
  req.on('data', data => {
    buffer += decoder.write(data)
  })
  req.on('end', () => {
    buffer += decoder.end()

    let choosenHandler =
      typeof router[trimmedPath] !== 'undefined'
        ? router[trimmedPath]
        : handlers.notFound

    let data = {
      trimmedPath,
      queryStringObject,
      method,
      headers,
      payload: buffer
    }

    choosenHandler(data, (statusCode = 200, payload = {}) => {
      let payloadString = JSON.stringify(payload)
      res.writeHead(statusCode)
      res.end(payloadString)

      console.log('Returning this response:', statusCode, payloadString)
    })
  })
})

server.listen(3000, () => {
  console.log(`The server is listening on port 3000 now`)
})
